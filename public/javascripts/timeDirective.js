angular.module("docsTimeDirective",[])
.controller("Controller",["$scope",function($scope){
  $scope.format = "M/d/yy h:mm:ss a";
}])
.directive("myCurrentTime",["$interval","dateFilter",function($interval,dateFilter){

  function link(scope,elm,attrs){
    var format;
    var timeoutId;

    function updateTime(){
      elm.text(dateFilter(new Date(),format));
    }

    scope.$watch(attrs.myCurrentTime, function(value){
      format = value;
      updateTime();
    });

    elm.on('$destroy',function(){
      console.log(timeoutId);
      $interval.cancel(timeoutId);
    });

    timeoutId = $interval(function(){
      updateTime();
    },1000);
  }

  return {
    link: link
  };
}]);