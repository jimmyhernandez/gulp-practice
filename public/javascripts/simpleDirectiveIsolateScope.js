angular.module("SimpleDirectiveIsoApp",[])
.controller("SimpleDirectiveIsoController",["$scope",function($scope){
  
  $scope.names = [];
  $scope.naomi = {
    name: "Naomi",
    address: "435 Boulevard"
  };
  $scope.igor = {
    name: "Igor",
    address: "435 La concorde"
  };

  $scope.names.push($scope.naomi);
  $scope.names.push($scope.igor);
}])
.directive("myCustomer",function(){
  return {
    restrict: "E",
    scope: {
      customerInfo: "=info"
    },
    templateUrl: "myCustomerIso.html"
  };
});