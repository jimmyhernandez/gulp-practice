angular.module("SimpleDirectiveApp",[])
.controller("SimpleDirectiveController",["$scope",function($scope){
  $scope.customer = {
    name: "Naomi",
    address: "435 Boulevard"
  };
}])
.directive("myCustomer",function(){
  return {
    restrict: "E",
    templateUrl: "myCustomer.html"
  };
});