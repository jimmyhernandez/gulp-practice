angular.module("scopeExample",[])
.controller("MyController",["$scope",function($scope){
  $scope.userName = "World";

  $scope.sayHello = function(){
    $scope.greeting = "Hello " + $scope.userName + "!";
  };
}]);