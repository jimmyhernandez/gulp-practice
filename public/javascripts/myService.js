angular.module("MyServiceApp",[])
.service("myService",function(){
  var myService = {};

  function add(num1,num2){
    return num1 + num2;
  }

  myService.add = add;

  return myService;
})
.controller("MyController",["$scope","myService",function($scope,myService){
  console.log(myService.add(1,2));
}]);