angular.module("ScopeInheritance",[])
.controller("MainController",["$scope",function($scope){
  $scope.timeOfDay = "morning";
  $scope.name = "Nikky";
}])
.controller("ChildController",["$scope",function($scope){
  $scope.name = "Mathias";
}])
.controller("GrandChildController",["$scope",function($scope){
  $scope.timeOfDay = "evening";
  $scope.name = "Gingerbread Baby";
  $scope.newTest = "Test Title";
}]);