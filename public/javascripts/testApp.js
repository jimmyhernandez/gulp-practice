angular.module("MyApp",[])
  .factory("MyTestService",[function(){
    return function(){
      console.log("My Test Factory");
    };
  }])
  .controller("MyController",["$scope","MyTestService",function($scope,MyTestService){
    console.log("Controller Test");
    MyTestService();
  }]);