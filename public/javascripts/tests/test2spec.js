describe("Test Notify Class",function(){
  var mock, notify;

  beforeEach(module("MyServiceModule"));

  beforeEach(function(){
    mock = {
      alert: jasmine.createSpy()
    };

    module(function($provide){
      $provide.value("$window",mock);
    });

    inject(function($injector){
      notify = $injector.get("notify1");
    });

  });

  it("should not alert first two notifications",function(){
    notify("one");
    notify("two");
    expect(mock.alert).not.toHaveBeenCalled();
  });

  it("should alert all after third notification",function(){
    notify("one");
    notify("two");
    notify("three");
    expect(mock.alert).toHaveBeenCalledWith("one\ntwo\nthree");
  });

  it("should clear the message after alert",function(){
    notify("one");
    notify("two");
    notify("three");
    notify("more");
    notify("two");
    notify("three");

    expect(mock.alert).toHaveBeenCalledWith("more\ntwo\nthree");
  });

});