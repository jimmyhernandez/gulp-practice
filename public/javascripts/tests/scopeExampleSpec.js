describe("Test Scope Example",function(){
  var $scope;
  beforeEach(module("scopeExample"));

  beforeEach(inject(function($rootScope, $controller){
    $scope = $rootScope.$new();
    $controller("MyController",{$scope: $scope});
  }));

  it("should declared the user name default value World",function(){
    expect($scope.userName).toEqual("World");
  });

  it("should say Hello World",function(){
    $scope.sayHello();
    expect($scope.greeting).toEqual("Hello World!");
  });
});