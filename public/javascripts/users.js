angular.module("api.users")
  .factory("Users",function(){
    var Users = {};

    var userList = [
      {
        id: "1",
        name: "Jane",
        role: "Designer",
        location: "New York",
        twitter: "gijane"
      },
      {
        id: "2",
        name: "Bob",
        role: "Developer",
        location: "New York",
        twitter: "billybob"
      },
      {
        id: "3",
        name: "Jim",
        role: "Developer",
        location: "New York",
        twitter: "jimbo"
      },
      {
        id: "4",
        name: "Bill",
        role: "Designer",
        location: "LA",
        twitter: "killbill"
      }
    ];

    Users.all = function(){
      return userList;
    };

    Users.findById = function(id){
      for(var i = 0; i < userList.length; i++){
        if(userList[i].id === id){
          return userList[i];
        }
      }
    };

    return Users;
  });