angular.module("EventListenerApp",[])
.directive("myDraggable",["$document",function($document){
  return {
    link: function(scope,elm,attrs){
      var startX = 0, startY = 0, x = 0, y = 0;

      elm.text("Drag Me");

      elm.css({
        position: "relative",
        border: "1px solid red",
        backgroundColor: "lightgrey",
        cursor: "pointer"
      });

      elm.on("mousedown",function(event){
        event.preventDefault();
        startX = event.pageX - x;
        startY = event.pageY - y;
        $document.on('mousemove', mousemove);
        $document.on('mouseup', mouseup);
      });

      function mousemove(event) {
        y = event.pageY - startY;
        x = event.pageX - startX;
        elm.css({
          top: y + 'px',
          left:  x + 'px'
        });
      }

      function mouseup() {
        $document.off('mousemove', mousemove);
        $document.off('mouseup', mouseup);
      }
    }
  };
}]);