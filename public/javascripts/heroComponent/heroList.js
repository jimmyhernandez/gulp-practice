function HeroListController($scope,$element,$attrs){
  var ctrl = this;
  //Get the HeroListController
  console.log(ctrl);

  ctrl.list = [
    {
      name: "Superman",
      location: "Mars"
    },
    {
      name: "Batman",
      location: "Bat Cave"
    }
  ];

  ctrl.updateHero = function(hero,prop,value){
    hero[prop] = value;
  };

  ctrl.deleteHero = function(hero){
    var idx = ctrl.list.indexOf(hero);
    if(idx >= 0){
      ctrl.list.splice(idx,1);
    }
  };

}

angular.module("HeroApp")
.component("heroList",{
  templateUrl: "heroList.html",
  controller: HeroListController
});