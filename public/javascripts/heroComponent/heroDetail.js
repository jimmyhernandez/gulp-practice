function HeroDetailController(){
  var ctrl = this;

  ctrl.delete = function(){
    ctrl.onDelete({hero: ctrl.hero});
  };

  ctrl.update = function(prop,value){
    ctrl.onUpdate({hero: ctrl.hero, prop: prop, value: value});
  }
} 

angular.module("HeroApp")
.component("heroDetail",{
  templateUrl: "heroDetail.html",
  controller: HeroDetailController,
  bindings: {
    hero: "<",
    onDelete: "&",
    onUpdate: "&"
  }
});