angular.module("NotyApp",[])
.directive("myNoty",function(){
  return {
    scope: {
      title: "@",
      message: "@",
      layout: "@",
      type: "@"
    },
    link: function(scope,elm,attrs){
      elm.text(scope.title);

      elm.css({
        position: 'relative',
        border: '1px solid green',
        background: "linear-gradient(green, white)",
        cursor: 'pointer'
      });

      elm.on("click",function(event){
        var n = noty({
          type: scope.type,
          layout: scope.layout,
          text: scope.message
        });
      });
    }
  };
});