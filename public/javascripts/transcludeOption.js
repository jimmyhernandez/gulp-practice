angular.module("transcludeApp",[])
.controller("TranscludeController",["$scope","$timeout",function($scope,$timeout){
  $scope.name = "Tobias";
  $scope.message = "";
  $scope.hideDialog = function(message){
    $scope.message = message;
    $scope.dialogIsHidden = true;
    $timeout(function(){
      $scope.message = "";
      $scope.dialogIsHidden = false;
    },1000);
  };
}])
.directive("myDialog",function(){
  return {
    restrict: "E",
    transclude: true,
    scope: {
      "close": "&onClose"
    },
    templateUrl: "my-dialog-close.html"
  };
});