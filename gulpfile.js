var gulp = require("gulp");
var gutil = require("gulp-util");
var jshint = require("gulp-jshint");
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var concat = require("gulp-concat");
var server = require("gulp-express");
var KarmaServer = require("karma").Server;

gulp.task("default",["server","watch","bdd"]);

gulp.task("jshint",function(){
  return gulp.src("public/javascripts/*.js")
  .pipe(jshint())
  .pipe(jshint.reporter("jshint-stylish"));
});

gulp.task("build-js",function(){
  return gulp.src("public/javascripts/*.js")
  .pipe(sourcemaps.init())
  .pipe(concat(build.js))
  .pipe(gutil.env.type === "production" ? uglify() : gutil.noop())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest("public/javascripts"))
});

gulp.task("build-css",function(){
  return gulp.src("public/stylesheets/src/*.scss")
  .pipe(sourcemaps.init())
  .pipe(sass())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest("public/stylesheets"));
});

gulp.task("server",function(){
  server.run(["server.js"]);
});

gulp.task("bdd",function(done){
  new KarmaServer({
    configFile: __dirname + "/karma.conf.js",
    singleRun: false
  },done).start();
});

gulp.task("watch",function(){
  gulp.watch("views/*.html",["server"]);
  gulp.watch("public/javascripts/*.js",["jshint","server"]);
  gulp.watch("public/stylesheets/src/*.scss", ["build-css","server"]);
});