var express = require("express");
var app = express();

//Serving static files in express
app.use("/static",express.static("public"));
app.use("/libs",express.static("node_modules"));
app.use("/",express.static("views"));

/*app.get("/",function(req,res){
    res.send("Hello World");
});*/

app.listen(3000,function(req,res){
    console.log("Example app listening on port 3000!");
});